Hadoop Ecosystem
    
    This project implements multiple tools based on Hadoop. Hadoop will be installed on Linux environment on windows using Docker
    and Kubernetes containers. Multi-node cluster Hadoop will be installed logically on a machine. As the tools are configured a 
    project will be implemented to test the working of the tools.

Getting Started

    Hadoop will be installed on docker and Kubernetes containers. And tools will be installed and running on top of it. Mainly 
    PySpark for stream processing, ELK stack (consisting of Elasticsearch, Logstash, and Kibana), Apache Beam, etc. Exploring 
    Hadoop tools dashboard, Hue for different frameworks.

Prerequisites
    
    Prior exposure to python programming(as we are using PySpark), networking, database concepts, and any of the Linux operating
    system flavors.

Trello Board 
    
    It's a project management application based on agile board for status tracking, consisting of Epics, Features, and Stories.
    Link: https://trello.com/b/jNlVpPqz